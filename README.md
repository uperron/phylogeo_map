### Phylogeography Sampling Bias maps
=========

Phylogeographic inference allows reconstruction of past geographical spread of pathogens or living organisms by integrating genetic and geographic data.
A popular model in continuous phylogeography -- with location data provided in the form of latitude and longitude coordinates -- describes spread as a Brownian motion (Brownian Motion Phylogeography, BMP) in continuous space and time, akin to similar models of continuous trait evolution.
Here, we show that reconstructions using this model can be strongly affected by sampling biases, such as the lack of sampling from certain areas.
As an attempt to reduce the effects of sampling bias on BMP, we consider the addition of sequence-free "mock" samples from under-sampled areas.
While this approach alleviates the effects of sampling bias, in most scenarios this will not be a viable option due to the need for prior knowledge of an outbreak's spatial distribution.
We therefore consider an alternative model, the spatial $\Lambda$-Fleming-Viot process ($\Lambda$FV), which has recently gained popularity in population genetics.
Despite the $\Lambda$FV robustness to sampling biases, we find that the different assumptions of the $\Lambda$FV and BMP models result in different applicabilities, with the $\Lambda$FV being more appropriate for scenarios of endemic spread, and BMP being more appropriate for recent outbreaks or colonizations.

Kalkauskas, A., Y. Sun, Perron, U., G. Baele, S. Guindon, N. Goldman, and N. De Maio (n.d.). “Sampling Bias and Model Choice in Continuous Phylogeography: Getting Lost on a Random Walk”. manuscript in preparation.

